"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.requests = {
    data: {
        createEntity: 'data.createEntity',
        project: 'data.project',
        getEntities: 'data.getEntities',
        getEntityById: 'data.getEntityById',
        addTagsToEntity: 'data.addTagsToEntity',
        removeTagsFromEntity: 'data.removeTagsFromEntity',
        getTags: 'data.getTags',
        updateTag: 'data.updateTag',
        getAppData: 'data.getAppData',
        updateAppData: 'data.updateAppData',
        uploadFile: 'data.uploadFile',
    },
    toast: {
        show: 'toast.show',
    },
    menu: {
        populate: 'menu.populate'
    },
    events: {
        menuItemClicked: 'menu-item-clicked',
        // TODO: this is for backwards compatibility
        // it is deprecated and will be removed in a future update
        menuClicked: 'menu-item-clicked'
    }
};
//# sourceMappingURL=requests.js.map